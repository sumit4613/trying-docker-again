FROM python:3.7-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /src
WORKDIR /src

# install dependencies
# we use --system flag because we don't need extra virtualenv

RUN pip install pipenv
COPY ./Pipfile  /src
RUN pipenv install --system --skip-lock

# copy our project code 
COPY . /src
#RUN python manage.py collectstatic --no-input
# expose the port 8000
EXPOSE 8000

# default command to run 
#CMD ["gunicorn", "--chdir", "mysite", "--bind", ":8000", "mysite.wsgi:application"]
CMD python manage.py collectstatic --no-input && python manage.py makemigrations && python manage.py migrate && gunicorn mysite.wsgi -b 0.0.0.0:8000