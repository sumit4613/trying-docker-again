# Setting up django project (including nginx, gunicorn, postgres) with docker
# on gitlab and digital ocean

## Create your django project
### Then update it with Dockerfile and docker-compose.yml file
### Then build it using `docker-compose up -d --build`

# On digital ocean
## Create new droplet, then inside the console of that droplet
# Install your git-lab runner using following commands:
## Simply download one of the binaries for your system:
### 1) `sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64`
## Give it permissions to execute:
### 2) `sudo chmod +x /usr/local/bin/gitlab-runner`
## Install Docker
### 3) `curl -sSL https://get.docker.com/ | sh`
## Create a GitLab CI user
### 4) `sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash`
## Install and run as service:
### 5) `sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner`
### 6) `sudo gitlab-runner start`
# Register that git-lab runner using following commands:
## Run the following command:
### 7) `sudo gitlab-runner register`
## 7th command will ask for gitlab instance url 
### Enter `https://gitlab.com`
## Enter the token you obtained to register the Runner inside your repositories setting under runner tab:
### `Enter that token` 
## Enter a description for the Runner, you can change this later in GitLab’s UI:
### Enter the name of your runner for eg- <my-runner>
## Enter the tags associated with the Runner, you can change this later in GitLab’s UI:
### You can enter the tags, anything you like
## Enter the Runner executor:
### It will ask for executor you can enter anything you like, I chose <shell>
## Then add docker to the gitlab-runner
### `sudo usermod -aG docker gitlab-runner`
## Add gitlab-runner to the sudoers
### `sudo nano /etc/sudoers`
## In sudoers file, under user privilege specification add,
### `gitlab-runner ALL=(ALL) NOPASSWD: ALL`
## Then restart the runner using:
### `sudo gitlab-runner restart`
## After that just add your gitlab-ci.yml file and write your commands in there.

#EDIT
